<!DOCTYPE html>
<html>
    <head>
            <meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
			<title>Atomic Project</title>

			<!-- Bootstrap -->
			<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
			<link href="css/style.css" rel="stylesheet">

			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
    </head>
    <body>
	<div class="container">
        <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt><span>Name:</span></dt>
			<dd>Pavel Parvej</dd>
            
            <dt>SEIP ID:</dt>
			<dd>SEIP 106611</dd>
            
            <dt>Batch:</dt>
			<dd>11</dd>
        </dl>
        <h2>Projects</h2>
        <table class="table table-bordered table-hover text-center bg-info">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
						<td><a href="./Views/Birthday/index.php">Birthday List</a></td>
				</tr>
					
                <tr>
                    <td>02</td>
                    <td><a href="./Views/Book/index.php">Favourite Books List</a></td>
                </tr>
				
				<tr>
                    <td>03</td>
						<td><a href="./Views/Checkbox_Condition/index.php">Terms & Condition</a></td>
				</tr>
				
				<tr>
                    <td>04</td>
						<td><a href="./Views/Checkbox_Hobby/index.php">Hobby List</a></td>
				</tr>
				
				<tr>
                    <td>05</td>
						<td><a href="./Views/City/index.php">City Selection</a></td>
				</tr>
				
				<tr>
                    <td>06</td>
						<td><a href="./Views/Education/index.php">Education Level</a></td>
				</tr>
				
				<tr>
                    <td>07</td>
						<td><a href="./Views/Email/index.php">Email Subscriptions</a></td>
				</tr>
				
				<tr>
                    <td>08</td>
						<td><a href="./Views/Summary/index.php">Summary of Organization</a></td>
				</tr>
				
				<tr>
                    <td>09</td>
						<td><a href="./Views/Profile/index.php">Profile Picture</a></td>
				</tr>
				
            </tbody>
        </table>
		
	</div>	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
        
    </body>
</html>
