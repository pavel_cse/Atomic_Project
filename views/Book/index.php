<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap Test</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div>
 <?php 
  include_once('../../vendor/autoload.php');
  use Project\Bitm\SEIP106611\Book\Book;

            $my_book = new Book();

            $my_book->create();
?>
  </div>
  
<div class="container">
  <h2>List of Books</h2>                           
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Book Name</th>
		<th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Java</td>
			<td>   
		    <a class="btn" href="java.php">View</a>	
            <a class="btn" href="edit1.php">Edit</a>
	        <a class="btn" href="delete1.php">Delete</a>
	     </td>
      </tr>
      <tr>
        <td>2</td>
        <td>Php</td>
			<td>   
		    <a class="btn" href="php.php">View</a>	
            <a class="btn" href="edit2.php">Edit</a>
	        <a class="btn" href="delete2.php">Delete</a>
	     </td>
      </tr>
      <tr>
        <td>3</td>
        <td>CSS</td>
			<td>   
		    <a class="btn" href="css.php">View</a>	
            <a class="btn" href="edit3.php">Edit</a>
	        <a class="btn" href="delete3.php">Delete</a>
	     </td>
      </tr>
    </tbody>
  </table>
  
  
  <label class="col-sm-6 control-label" for="formGroupInputSmall"></label>
    <label class="col-sm-6 control-label" for="formGroupInputSmall">
	<nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
	<a class = "btn btn-info "href="create.php">Create New</a>
  </ul>
  
</nav>

	</label>

    




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>