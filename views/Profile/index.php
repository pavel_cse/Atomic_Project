<!DOCTYPE HTML>
<html lang="en-US">
<head>
            <meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
			<title>Profile Picture</title>

			<!-- Bootstrap -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="style.css" rel="stylesheet">

			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
</head>
<body>
	<div>
		<?php 
		  include_once('../../vendor/autoload.php');
		  use Project\Bitm\SEIP106611\Profile\Profile;

			$my_Summary = new Profile();

			$my_Summary->create();
		?>
	</div>
<div class="container">

</div>
<div class="container">
	<h4>Edit your Picture</h4>
	<form id="profilepic_form">
	

		<div style ="width: 200px; height: 100px; position: relative; background-color: #fbfbfb;   border: 1px solid #b8b8b8; "data-controltype="image">
		<img src="img/pic.png" alt="image" style="position:     absolute; top: 50%; left: 50%; margin-left: -16px; margin-top: -18px">

		<div style= "top:50px;"data-role="fieldcontain" data-controltype="camerainput">
		<input type="file" name="" id="camerainput1" accept="image/*" capture="camera">
		</div>
		</div>
		<div>
		<input type="submit" value="Save Change" />
		</div>
	</form>
</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>